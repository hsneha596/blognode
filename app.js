const path = require('path');

const express = require('express');
const bodyParser = require('body-parser')
const errorController = require('./controllers/error')
const app = express();

app.set('view engine', 'ejs');
app.set('views', 'views');

const adminRoutes = require('./routes/admin')
const shopRoutes = require('./routes/shop')

app.use(bodyParser.urlencoded({ extended: false })); //html-body parser
//for css
app.use(express.static(path.join(__dirname, 'public')));

app.use('/admin', adminRoutes.routes); //only routes starting with '/admin' will go into adminRoutes
app.use(shopRoutes);


// app.use((req, res, next) => {
//     //join constructs and returns a path, a global var- __dirname - absolute path on OS to this proj folder, name of the views folder, html file
//     res.status(404).sendFile(path.join(__dirname, 'views', '404.html'));
// });

app.use(errorController.get404);

app.listen(3000)
