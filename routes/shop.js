const path = require('path');
const express = require('express');
const rootDir = require('../util/path');
const adminData = require('./admin');
const router = express.Router();
const productController = require('../controllers/products')

router.get('/', productController.getProducts
    // (req, res, next) => {
    // res.send('<h1>Hello from Express!</h1>');

    //     const products = adminData.products;
    //     res.render('shop', {
    //         prods: products,
    //         pageTitle: 'Shop',
    //         path: '/',
    //         hasProducts: products.length > 0,
    //         activeShop: true,
    //         productCSS: true
    //     });
    // }
);

module.exports = router;