const express = require('express')

const router = express.Router()
//import controller
const productsController = require('../controllers/products')


// /admin/add-product => // admin gets added auto config in app.js if you had to call /admin/add-product and /admin/product no need to // explicitly add it in lin 6 and 11 directly have it in app.js
// router.get('/add-product', (req, res, next) => {
//     console.log(req.body);
//     res.render('add-product', {
//         pageTitle: 'Add-Product',
//         path: '/admin/add-product',
//         formCSS: true,
//         productCSS: true,
//         activeAddProduct: true
//     })
// })

router.get('/add-product', productsController.getAddProduct)

module.exports = router