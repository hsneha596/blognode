const path = require('path');

const express = require('express');
const rootDir = require('../util/path');
const router = express.Router();
const productController = require('../controllers/products')
// const products = [];
// /admin/add-product => // admin gets added auto config in app.js if you had to call /admin/add-product and /admin/product no need to // explicitly add it in lin 6 and 11 directly have it in app.js
// /admin/add-product => GET
router.get('/add-product',
    //  (req, res, next) => {
    // res.send(
    //     '<form action="/admin/add-product" method="POST"><input type="text" name="title"><button type="submit">Add Product</button></form>'
    // );
    // res.render('add-product', {
    //     pageTitle: 'Add Product',
    //     path: '/admin/add-product',
    //     formsCSS: true,
    //     productCSS: true,
    //     activeAddProduct: true
    // });
    //call it from controllers
    productController.getAddProduct
    // }
);

// /admin/add-product => POST
router.post('/add-product', productController.postAddProduct)

exports.routes = router;
// exports.products = products;